import React,{ useState, useEffect } from "react";
import "./EditRequest.css";
import Header from "../CommonComponent/Header.js";
import Footer from "../CommonComponent/Footer.js";
import { useParams } from 'react-router-dom'
import DemandTrackerServices from "../../services/DemandTrackerServices";
import ToastrMessage from '../CommonComponent/ToastrMessage';
import 'react-toastify/dist/ReactToastify.css';
import config from "../../config/config.js";
import { useHistory } from 'react-router-dom';


export default function EditRequest() {
    const history = useHistory();
    const { id } = useParams();
    const [gradeData, setGradeData] = useState([]);
    const [form, setForm] = useState({
        reqId:'',
        clusterId: '',
        subClusterId: '',
        gradeId: '',
        techStack: '',
       coreSkill: '',
        skillDetails: '',
        startDate: '',
        createdDate: '',
        ownerId: '',
        reasonForDemand: '',
        workLocation: '',
        area: '',
        status: '',
        soNumber: '',
        comment:''
    });
    const demandTrackerServices = new DemandTrackerServices();
    const [modifyButton, setModifyButton] = useState(true);
    const [saveButton, setSaveButton] = useState(false);
    const [withdrawButton, setWithdraw] = useState(true);
    const [withdrawStatus, setWithdrawStatus] = useState(false);
    const [cancelButton, setCancelButton] = useState(false);
    const buttonDisplay = (event) => {
        if (event.target.id == 'modifyButton') {
            setModifyButton(false);
            setSaveButton(true);
            setWithdraw(false);
            setCancelButton(true);
        }
        if (event.target.id == 'cancelButton') {
            demandTrackerServices.getRequest(`get/${id}`)
            .then((res) => {
                setForm(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
            setModifyButton(true);
            setSaveButton(false);
            setWithdraw(true);
            setCancelButton(false);
        }
    };
    useEffect(() => {
        demandTrackerServices.getRequest(`get/${id}`)
            .then((res) => {
                console.log(res);
                setForm(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
        // axios.get("http://localhost:8000/request").then(res => {
        //     setForm(res.data);
        // }).catch((err) => {
        //     console.log(err);
        // })
        // console.log("Initialy it is working",id);
        demandTrackerServices.getRequest('grade/getAll').then(
            res => {
                setGradeData(res.data);
            }
        ).catch((err) => {
            console.log(err);
        })
        if(form.status === 'withdraw'){
            setWithdrawStatus(true);
        }
    }, []);

    const handleGradeChange = (event) => {
        setForm({...form, [event.target.name] : event.target.value});
    }
    const disableFields = (status) => {

    }
    const handleDemandChange = (event) => {
        setForm({...form, [event.target.name] : event.target.value});
    }
    const handleLocationChange = (event) => {
        setForm({...form, [event.target.name] : event.target.value});
    }
    const handleAreaChange = (event) => {
        setForm({...form, [event.target.name] : event.target.value});
    }
    const handleSkillChange = (event) => {
        setForm({...form, [event.target.name] : event.target.value});
    }
    const handleStackChange = (event) => {
        setForm({...form, [event.target.name] : event.target.value});
    }
    const handleSkillDetailChange = (event) => {
        // data.skillDetails = event.target.value;
        setForm({...form, [event.target.name] : event.target.value});
        console.log(event.target.value);
    }
    // const handleChange = (event) => {
    //     setEditField({ ...editField, [event.target.name]: event.target.value });
    // };
    
    
    

    const handleSubmit = (event) => {
        event.preventDefault();
        const editReq = {
            reqId : form.reqId,
            clusterId: form.clusterId,
            subClusterId: form.subClusterId,
            gradeId: form.gradeId,
            techStack: form.techStack,
            coreSkill: form.coreSkill,
            skillDetails: form.skillDetails,
            startDate: form.startDate,
            createdDate: form.createdDate,
            ownerId: form.ownerId,
            reasonForDemand: form.reasonForDemand,
            workLocation: form.workLocation,
            area: form.area,
            status: form.status,
            soNumber: form.soNumber,
            comment: form.comment
        };
        /* sending updated request*/
        console.log(editReq);
         demandTrackerServices.updateRequest('upd', editReq).then(
            (res) => {
                // console.log("jalsdkfj",res);
                ToastrMessage.notify(editReq.reqId + ': Request Modified Successfully.', config.toasterPos, config.SUCCESS);
                setTimeout(() => {
                    history.push('/reqList');
                }, 2500);
                // console.log("toaster worked");

            }
        ).catch((err) => {
            ToastrMessage.notify(editReq.reqId + ' : Request Not Modified .', config.toasterPos, config.ERROR);
            console.log(err);
        });

    };

    const handleWithdraw = (event) => {
        const editReq = {
            reqId : form.reqId,
            clusterId: form.clusterId,
            subClusterId: form.subClusterId,
            gradeId: form.gradeId,
            techStack: form.techStack,
            coreSkill: form.coreSkill,
            skillDetails: form.skillDetails,
            startDate: form.startDate,
            createdDate: form.createdDate,
            ownerId: form.ownerId,
            reasonForDemand: form.reasonForDemand,
            workLocation: form.workLocation,
            area: form.area,
            status: 'withdraw',
            soNumber: form.soNumber,
            comment: form.comment
        };
        demandTrackerServices.updateRequest('upd', editReq).then(
            (res) => {
                console.log(res);
                ToastrMessage.notify(res.data.reqId + ': Request Withdrawn Successfully.', config.toasterPos, config.SUCCESS);
                setTimeout(() => {
                    history.push(`/editRequest/${res.data.reqId}`);
                }, 2500);

            }
        ).catch((err) => {
            ToastrMessage.notify(form.reqId + ': Request Withdraw Unsuccessful.', config.toasterPos, config.ERROR);
            console.log(err);
        });

    }

    return (
        <div>
            <Header> </Header>
            <div className="col-lg-12 parent-container">
                <div className="container-fluid well">
                    <div
                        className="row text-light"
                        style={{ backgroundColor: "#1434A4" }}
                    >
                        <div
                            className="col-lg-12 mt-1"
                            style={{ width: "170px", fontSize: "large" }}
                        >
                            <i className="fas fa-align-justify"> </i>&nbsp;Modify Request
                        </div>
                    </div>
                    <div className="box">
                        <form action="#">
                            <div className="form">
                                <div className="component">
                                    <input
                                        className="form-control form-control-sm"
                                        type="text"
                                        name="reqId"
                                        value={form.reqId}
                                        placeholder="Request ID"
                                        disabled
                                    >

                                    </input>
                                </div>
                                <div className="component">
                                    <input
                                        className="form-control form-control-sm"
                                        id="cluster"
                                        name="cluster"
                                        value={form.clusterId}
                                        placeholder="Cluster ID"
                                        disabled
                                    />
                                </div>
                                <div className="component">
                                    <input
                                        className="form-control form-control-sm"
                                        name="subCluster"
                                        value={form.subClusterId}
                                        placeholder="Sub Cluster ID"
                                        disabled
                                    />
                                </div>
                                <div className="component">
                                    { modifyButton?    
                                        <input
                                            className="form-control  form-control-sm"
                                            name="gradeId"
                                            value={form.gradeId}
                                            placeholder="Grade ID"
                                            disabled
                                        />
                                        :
                                        <select className="form-select  form-select-sm" name='gradeId' value={form.gradeId} onChange={handleGradeChange}>
                                            <option defaultValue=''>Select Grade</option>
                                            {
                                                gradeData.map((object, i) => {
                                                    return <option key={i} value={object}>{object}</option>
                                                })
                                            }
                                        </select>
                                    }
                                </div>
                                
                                <div className="component">
                                    <input
                                        className="form-control form-control-sm"
                                        type="text"
                                        size="15"
                                        placeholder="Technology Stack"
                                        name="techStack"
                                        value={form.techStack}
                                        onChange={handleStackChange}
                                        disabled = {modifyButton}
                                    />
                                </div>
                                <div className="component">
                                    <input
                                        className="form-control form-control-sm"
                                        type="text"
                                        size="20"
                                        placeholder="Core Skill"
                                        name="coreSkill"
                                        value={form.coreSkill}
                                        onChange={handleSkillChange}
                                        disabled = {modifyButton}
                                    />
                                </div>
                                <div className="component text-area">
                                    <textarea
                                        className="form-control form-control-sm textareaSize"
                                        name="skillDetails"
                                        placeholder="Skill Details"
                                        value={form.skillDetails}
                                        onChange = {handleSkillDetailChange}
                                        disabled = {modifyButton}
                                    >

                                    </textarea>
                                </div>
                                <div className="component startDate">
                                    <input
                                        className="form-control form-control-sm"
                                        type="text"
                                        name="startDate"
                                        placeholder="Start Date"
                                        value={form.startDate}
                                        disabled
                                    />
                                </div>
                                <div className="component createdDate">
                                    <input
                                        type="text"
                                        className="form-control form-control-sm"
                                        defaultValue={form.createdDate}
                                        placeholder="Created Date"
                                        disabled
                                    />
                                </div>
                                <div className="component">
                                    <input
                                        size="15"
                                        className="form-control form-control-sm"
                                        type="text"
                                        defaultValue={form.ownerId}
                                        placeholder="Owner ID"
                                        disabled
                                    />
                                </div>
                                <div className="component">
                                    { modifyButton?
                                        <input
                                            className="form-control form-control-sm"
                                            value={form.reasonForDemand}
                                            name="reasonForDemand"
                                            placeholder="Reason For Demand"
                                            disabled
                                        />
                                        :
                                        <select className="form-select form-select-sm" value={form.reasonForDemand} name='reasonForDemand' onChange={handleDemandChange} autoComplete='off'>
                                            <option defaultValue='' >Reason for Demand</option>
                                            <option value='New'>New</option>
                                            <option value='Replacement'>Replacement</option>
                                        </select>
                                    }
                                </div>
                                <div className="component">
                                    { modifyButton?
                                        <input
                                            className="form-control form-control-sm"
                                            name="workLocation"
                                            value={form.workLocation}
                                            placeholder="Work Location"
                                            disabled
                                        />
                                        :
                                        <select className="form-select form-select-sm" name='workLocation' value={form.workLocation} onChange={handleLocationChange}>
                                            <option defaultValue=''>Work Location</option>
                                            <option value='Bengaluru'>Bengaluru</option>
                                            <option value='Hyderabad'>Hyderabad</option>
                                            <option value='Mumbai'>Mumbai</option>
                                            <option value='Noida'>Noida</option>
                                            <option value='Chennai'>Chennai</option>
                                        </select>
                                    }
                                </div>
                                <div className="component">
                                    {modifyButton?
                                        <input
                                            className="form-control form-control-sm"
                                            aria-label=".form-select-sm example"
                                            name="area"
                                            placeholder="Area"
                                            value={form.area}
                                            disabled = {modifyButton}
                                        />
                                        :
                                        <select className="form-select form-select-sm" aria-label=".form-select-sm example" name='area' value={form.area} onChange={handleAreaChange}>
                                            <option defaultValue=''>Select Area</option>
                                            <option value='SLF'>SLF</option>
                                            <option value='BD'>BD</option>
                                            <option value='GOV'>GOV</option>
                                        </select>
                                    }
                                </div>
                                <div className="component">
                                    {/* <select
                                        className="form-select form-select-sm"
                                        aria-label=".form-select-sm example"
                                        name="status"
                                        value={form.status}
                                        disabled
                                        // onChange={handleChange}
                                    >
                                        <option value="open"> Open </option>
                                        <option value="inprogress"> In Progress </option>
                                        <option value="pending"> Pending </option>
                                        <option value="onhold"> On Hold </option>
                                        <option value="withdraw"> Withdraw </option>
                                        <option value="closed"> Closed </option>
                                        <option value="cancelled"> Cancelled </option>
                                    </select> */}
                                    <input
                                        className="form-control form-control-sm"
                                        type="text"
                                        name="status"
                                        placeholder="Select status"
                                        value={form.status}
                                        disabled /*onChange={handleChange}*/
                                    >

                                    </input>
                                </div>
                                <div className="component soNum">
                                    <input
                                        className="form-control form-control-sm"
                                        type="text"
                                        name="SO"
                                        placeholder="Enter SO Number"
                                        value={form.soNumber}
                                        disabled /*onChange={handleChange}*/
                                    >

                                    </input>
                                </div>
                                <div className="component text-area2">
                                    <textarea
                                        className="form-control form-control-sm"
                                        name="comments"
                                        placeholder="Comment"
                                        value={form.comment}
                                        disabled
                                        // onChange={handleChange}
                                    >
                                    </textarea>
                                </div>
                            </div>
                            <div className="btns">
                                <div>                                    
                                        {
                                            withdrawStatus ?
                                            <h4>Withdraw</h4>
                                            :
                                            withdrawButton ?
                                                <button
                                                    type="reset"
                                                    className="btn btn-sm btn-danger reset"
                                                    id="withdrawButton"
                                                    onClick={handleWithdraw}
                                                >
                                                    <i className="fas fa-eject"></i>&nbsp;Withdraw
                                                </button>
                                                : null
                                        }

                                        {
                                            cancelButton ?
                                                <button
                                                    type="reset"
                                                    className="btn btn-sm btn-info reset"
                                                    id="cancelButton"
                                                    onClick={buttonDisplay}
                                                >
                                                    <i className="fas fa-window-close"></i>&nbsp;Cancel
                                                </button>
                                                : null
                                        }

                                        {   withdrawStatus?
                                                <button
                                                type="submit"
                                                className="btn btn-sm btn-primary submit"
                                                >
                                                <i className="fas fa-save"></i>&nbsp;Resubmit
                                                </button>
                                        
                                            :
                                            saveButton ?
                                                <button
                                                    type="reset"
                                                    className="btn btn-sm btn-success reset"
                                                    onClick={handleSubmit}
                                                >

                                                    <i className="fas fa-save"></i>&nbsp;Save
                                                </button>
                                                : null
                                        }

                                        {
                                            withdrawStatus?
                                                null
                                            :
                                                modifyButton ?
                                                    <button
                                                        type="submit"
                                                        className="btn btn-sm btn-primary submit"
                                                        id="modifyButton"
                                                        onClick={buttonDisplay}
                                                    >
                                                        <i className="fas fa-check-square"> </i>&nbsp;Modify
                                                    </button>
                                                    :
                                                    null
                                        }
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <ToastrMessage></ToastrMessage>
            </div>
            <Footer> </Footer>
        </div>
    );
}
