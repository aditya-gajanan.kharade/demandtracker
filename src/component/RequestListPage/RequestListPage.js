import React from 'react'
import { useHistory } from 'react-router'
import FloadingButton from '../FloatingButton/FloatingButton';
import Header from '../CommonComponent/Header';
import './ReqListPage.css'

function RequestListPage() {
    const history = useHistory()

    return (
        <div>
            <header>
                <Header></Header>
            </header>

            <section id='parentSection'>
                <section id='filter'>
                    <h1>Filters Part</h1>
                </section>
                <section id='search'>
                    <h1>Search Bar will be displaying here</h1></section>
                <article id='reqLists'>
                    <h1>All Requests will be displaying here</h1>
                    <FloadingButton></FloadingButton>
                </article>

            </section>
        </div>
    )
}

export default RequestListPage
