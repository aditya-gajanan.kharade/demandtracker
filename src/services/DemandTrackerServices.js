import axios from 'axios';
import config from '../config/config';
export default class DemandTrackerServices {
    baseUrl = config.baseUrl;
    createRequest(url,data) {
        return axios.post(this.baseUrl+url,data);
    }

    getRequest(url) {
        return axios.get(this.baseUrl+url);
    }
    updateRequest(url, data){
        return axios.post(this.baseUrl+url, data);
    }
}